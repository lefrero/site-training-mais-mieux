var acceuil =  document.querySelector(".acceuil span")
var portfolio =  document.querySelector(".portfolio span")

let fontcolor = "white"
let backgroundcolor = "black"

var wheel = document.getElementById("wheel")
window.addEventListener('scroll', function(e) {
    wheel.style.transform = "rotate("+document.body.scrollTop/4+"deg)";
    acceuil.style.left = -document.body.scrollTop * 1 + 'px';
    portfolio.style.left = -document.body.scrollTop * 1 + 'px';


});
var color = document.getElementById("color")
var back = false
color.addEventListener('click', function (e) {
    if (back) {
        back = false;
        fontcolor = "white";
        backgroundcolor = "black";

    }
    else {
        back = true;
        fontcolor = "black";
        backgroundcolor = "white";
    }
    document.body.style.cssText = "color :"+fontcolor + "!important";
    document.body.style.backgroundColor = backgroundcolor;
    document.querySelectorAll("#nomsoc svg line").forEach(e => e.style.stroke = fontcolor);
    color.style.backgroundColor = fontcolor;
})



//annimations


//Fade in bottom
const fadeInBottom = new IntersectionObserver(entries => {
    entries.forEach(entry=> {
        console.log(entry.target)
        if (entry.isIntersecting) {
            entry.target.classList.add("fadeInBottom");
            return
        }
    });
});
var classToAnimate = document.querySelectorAll('.in');
classToAnimate.forEach(element => fadeInBottom.observe(element));


//Fade in left
const fadeInLeft = new IntersectionObserver(entries => {
    entries.forEach(entry=> {
        if (entry.isIntersecting) {
            entry.target.classList.add("fadeInLeft");
            return
        }
    });
});
var classToAnimate2 = document.querySelectorAll('  #nomsoc > div, #nomsoc > svg');
classToAnimate2.forEach(element => fadeInLeft.observe(element));



//photos sites
var list = document.getElementById("nomsoc").querySelectorAll('h1');
var active = document.getElementById("precym");
var new_img = document.getElementById("img_display");
active.style.color = "#FFE226";
list.forEach(element =>
    element.addEventListener("click", function (e) {
        active.style.color = null;
        element.style.color = "#FFE226";
        active = document.getElementById(element.id);
        new_img.src = "img/"+element.id+".png";
        new_img.classList.add("fadeInright");
        setTimeout(function(){
            new_img.classList.remove("fadeInright");
        },1000);
    } )

);


//scrolling
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});
